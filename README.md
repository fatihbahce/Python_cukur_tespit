# Python Çukur Tespit

Dünyanın dört bir yanındaki belediyelerin karşılaştığı büyük bir sorun yollardaki çukurlar. Belediyeler yolların hasarsız olmasını sağlamak sorumluluğunda olsa da, bazen sorunu gözden kaçırıyorlar ve çoğu zaman sorunun varlığından bile habersizler. “Güvenlik Kaynak Merkezi”ne göre, sürücüler tarafından patlamış lastiklerin, patlamış aksların ve araçlarındaki diğer hasarların onarımı için yaklaşık 3 Milyar ABD Doları harcanmaktadır. "Amerikan Otomobil Birliği'nin (AAA)" bir makalesine göre, son beş yılda ABD genelinde yaklaşık 16 milyon sürücü çukurlardan zarar gördü.

Yol durumunu korumak, belediyeler için sürekli hava değişiklikleri, aşınma ve yıpranma, düşük bütçeler ile zorlu bir iştir. Ayrıca insanları bilgilendirmeyi unutmamak da bir görevdir. Yani, bu bahsedilen zorlukları çözmeyi amaçlayan bir uygulama. Birden fazla görüntü üzerinde nesne izleme eğitimi ile elde edildi ve evrişimli sinir ağları kullanılarak geliştirildi. Kendi yetki alanı tabanlı web/mobil uygulama tabanlı panolarını kullanarak iş emirlerini oluşturmak ve yönetmek için görüntüleyebilecekleri ve güncelleyebilecekleri en yakın ilgili yetkili için dinamik bir rapor da oluşturulur.

Şehir içi otobüslerinde yer alan ip kamera sayesinde görüntüler ana merkeze gönderiliyor. ana sunucuda bulunan çukur tespit programı bütün görüntüleri analiz ediyor, analiz sonucu tespit ettiği çukurlaru konumları ile birlikte ilgili birime gönderiyor.

Böylece %95 oranında doğruluk ile tespitler gerçekleşiyor ve çukurlara anlık olarak müdahale edilebiliyor.
 
